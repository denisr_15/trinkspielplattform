<?php
include '../php/header.php';
include '../php/connection.php';
?>
<div id="fh5co-main">
        <div class="fh5co-narrow-content animate-box" data-animate-effect="fadeInLeft">
<?php

if(isset($_POST['username']) && isset($_POST['password'])){
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT id, username, email, enabled FROM user WHERE username='$username' AND password='$password'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {

                if($row["enabled"] == 1) {

                    $_SESSION["userid"] = $row["id"];
                    $_SESSION["username"] = $row["username"];
                    $_SESSION["useremail"] = $row["email"];
                    $_SESSION["LogIn"] = True;

                    header("Location: ../HOME");
                } else {
                    echo "<h4 style='text-align: center; color: red;'>Couldn't log in. Username is not enable.</h4>";
                }
            }
        } else {
            echo "<h4 style='text-align: center; color: red;'>Couldn't log in. Username or password incorrect.</h4>";
        }

    } else {
        $username = "";
        $password = "";
    }

    
?>
            <form action="index.php" method="post">
                <div class="row login">
                    <h1>Login</h1>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <a href="resetPassword.php">Forgot your password?</a>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-md" value="Log in">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
include '../php/footer.php';
?>